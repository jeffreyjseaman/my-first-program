package application;
	
import java.io.IOException;

import application.Main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import application.Main;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	

	private static BorderPane mainlayout;
	private Stage primaryStage;
	private AnchorPane mainLayout;
	
	//Creating, initializing, and displaying our primary stage.
	//Displaying scene 1.
	@Override
	public void start(Stage primaryStage) throws IOException
	{
		
		
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Splash Screen");
		
		showSplashScreen();
		
	}
		
	public void showSplashScreen() throws IOException{
		FXMLLoader myLoader = new FXMLLoader();	
		myLoader.setLocation(Main.class.getResource("SplashScreen.fxml"));
		mainLayout = myLoader.load();
		Scene myscene = new Scene (mainLayout);
		primaryStage.setScene(myscene);
		primaryStage.show();
	}
public static void showHomeScreen() throws IOException{
		
		FXMLLoader myloader = new FXMLLoader();
		myloader.setLocation(Main.class.getResource("Scenes/HomeScreen.fxml"));
		AnchorPane Home = myloader.load();
		mainlayout.setCenter(Home);
		 
		 
	}


	
	public static void main(String[] args)
	{
		launch(args);
	}
}

				