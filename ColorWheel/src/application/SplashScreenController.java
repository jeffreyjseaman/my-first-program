package application;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;


public class SplashScreenController {
	static Main Main;
	public Button Enter;

	@FXML
	private void goHome() throws IOException{
		Main.showHomeScreen();
	}
	
}
