package Employees;

public class Employees {
//Declare instance variables
	private  int EmployeeNumber;
	private String EmployeeLastName;
	private String EmployeeFirstName;
	private double EmployeeSalary; 
	private String EmployeeLocation;
	
	//Getter
	public int getEmployeeNumber()
	{
		return EmployeeNumber;
	}
	
	//Setter
	public void setEmployeeNumber(int emp)
	{
		EmployeeNumber = emp;//This is the value that user sends us 
	}
	
	//Getter
	public String getEmployeeLastName()
	{
		return EmployeeLastName;
	}
	
	//Setter
	public void setEmployeeLastName(String LastName)
	{
		EmployeeLastName = LastName;
	}
	
	//Getter 
	public String getEmployeeFirstName()
	{
		return EmployeeFirstName;
	}
	
	//Setter
	public void setEmployeeFirstName(String FirstName)
	{
		EmployeeFirstName = FirstName;
	}
	
	//Getter
	public double getEmployeeSalary()
	{
		return EmployeeSalary;
	}
	
	//Setter
	public void setEmployeeSalary(double Salary)
	{
		EmployeeSalary = Salary;
	}
	
	//Getter
	public String getEmployeeLocation()
	{
		return EmployeeLocation;
	}
	//Setter
	public void setEmployeeLocation(String location)
	{
		EmployeeLocation = location;
	}
}
