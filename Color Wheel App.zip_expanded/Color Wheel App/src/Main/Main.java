package Main;
	
import java.io.IOException;

import javax.print.DocFlavor.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	Stage primaryStage;
	static BorderPane mainlayout;
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage = primaryStage;
		showSplashScreen();
	}
	public void showSplashScreen() throws IOException {
		FXMLLoader myLoader = new FXMLLoader();
		myLoader.setLocation(Main.class.getResource("Splash/SplashScreen.fxml"));
		mainlayout = myLoader.load();
		Scene myscene = new Scene(mainlayout);
		primaryStage.setScene(myscene);
		primaryStage.show();
	}
	
	public static void showHomeScreen() throws IOException {
		FXMLLoader myLoader = new FXMLLoader();
		myLoader.setLocation(Main.class.getResource("Home/HomeScreen.fxml"));
		BorderPane HomeScreen = myLoader.load();
		mainlayout.setCenter(HomeScreen);
		
		
	}
	public static void main(String[] args) {
		launch(args);
	}
}
	
	

