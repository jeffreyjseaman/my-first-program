package Controllers;

import java.awt.event.ActionEvent;
import java.io.IOException;

import java.net.URL;
import java.util.ResourceBundle;
import com.jfoenix.controls.*;

import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;


public class LoginController implements Initializable {
	
	@FXML
	private JFXButton signup;
	
	@FXML
	private JFXTextField username;
	
	@FXML
	private JFXCheckBox remember;
	
	@FXML
	private JFXButton login;
	
	@FXML
	private ImageView progress;
	
	@FXML
	private JFXPasswordField password;

	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		progress.setVisible(false);
		username.setStyle("fx-text-inner-color:#fbf6f6");
		password.setStyle("fx-text-inner-color:#fbf6f6");
	}
	
	
	@FXML
	public void loginAction(ActionEvent e)
	{
		progress.setVisible(true);
		PauseTransition pt = new PauseTransition();
		pt.setDuration(Duration.seconds(3));
		pt.setOnFinished(ev ->{
			System.out.println("Login Successfully");
			
		});
		
		
		pt.play();
	}
	@FXML
	public void signUp(ActionEvent el) throws IOException
	{
		login.getScene().getWindow().hide();
		
		Stage signup = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("SignUp.fxml"));
		Scene scene = new Scene (root);
		signup.setScene(scene);
		signup.show();
		signup.setResizable(false);
	}
}
